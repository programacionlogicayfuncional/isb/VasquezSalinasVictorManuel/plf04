(ns plf04.core)

(defn stringE-1
  [palabra]
  (letfn [(f [palabra2]
             (if (empty? palabra2) 0 (if (= (first palabra2) \e)
                  (+ 1 (f (rest palabra2)))
                  (f (rest palabra2))))
             )
          (f2 [x] (if (and (>= (f x) 1) (<= (f x) 3)) true false))]
    (f2 palabra)))

(stringE-1 "Hello")
(stringE-1 "Heelle")
(stringE-1 "Heelele") 
(stringE-1 "Hll")
(stringE-1 "e")
(stringE-1 "")

(defn stringE-2 
  [palabra] 
  (letfn [(f [palab acc]
             (if (empty? palab) acc (if (= (first palab) \e) (f (rest palab) (+ 1 acc)) (f (rest palab) acc))))
          (f2 [x y] (if (and (>= (f x y) 1) (<= (f x y) 3)) true false))
          ]
    (f2 palabra 0)))

(stringE-2 "Hello")
(stringE-2 "Heelle")
(stringE-2 "Heelele")
(stringE-2 "Hll")
(stringE-2 "e")
(stringE-2 "")

 (defn stringTimes-1
   [palabra n]
   (letfn [(f [palabra n]
             (if (= n 0) "" (cons palabra (f palabra (- n 1)))))]
     (f palabra n)))

(stringTimes-1  "Hi" 2)
(stringTimes-1 "Hi" 3)
(stringTimes-1 "Hi" 1)
(stringTimes-1 "Hi" 0)
(stringTimes-1 "Hi" 5)
(stringTimes-1 "Oh Boy!" 2)
(stringTimes-1 "x" 4)
(stringTimes-1 "" 4)
(stringTimes-1 "code" 2)
(stringTimes-1 "code" 3)

(defn stringTimes-2
  [palabra n]
  (letfn [(f [palabra n acc]
            (if (= n 0) acc (f palabra (- n 1) (concat acc palabra))))]
    (f palabra n nil)))

(stringTimes-2  "Hi" 2)
(stringTimes-2 "Hi" 3)
(stringTimes-2 "Hi" 1)
(stringTimes-2 "Hi" 0)
(stringTimes-2 "Hi" 5)
(stringTimes-2 "Oh Boy!" 2)
(stringTimes-2 "x" 4)
(stringTimes-2 "" 4)
(stringTimes-2 "code" 2)
(stringTimes-2 "code" 3) 

(defn frontTimes-1
  [palabra n]
  (letfn [(f [palabra n]
            (if (= n 0) ""
                (concat (str (first palabra) (first (rest palabra)) (first (rest (rest palabra)))) (f palabra (- n 1)))
                ))]
    (f palabra n)))

(frontTimes-1 "Chocolate" 2)
(frontTimes-1 "Chocolate" 3)
(frontTimes-1 "Abc" 3)
(frontTimes-1 "Ab" 4)
(frontTimes-1 "A" 4)
(frontTimes-1 "" 4)
(frontTimes-1 "Abc" 0)

(defn frontTimes-2
  [palabra n]
  (letfn [(f [palabra n acc]
            (if (= n 0) acc
                (f palabra (- n 1) (concat (str (first palabra) (first (rest palabra)) (first (rest (rest palabra)))) acc))))]
    (f palabra n "")))

(frontTimes-2 "Chocolate" 2)
(frontTimes-2 "Chocolate" 3)
(frontTimes-2 "Abc" 3)
(frontTimes-2 "Ab" 4)
(frontTimes-2 "A" 4)
(frontTimes-2 "" 4)
(frontTimes-2 "Abc" 0)

(defn countXX-1
  [palabra]
  (letfn [(f [palabra]
            (if (empty? palabra) 0
                (if (and (= (first palabra) \x) (= (first (rest palabra)) \x))
                  (+ 1 (f (rest palabra))) (f (rest palabra)))))]
    (f palabra)))

(countXX-1 "abcxx")
(countXX-1 "xxx")
(countXX-1 "xxxx")
(countXX-1 "abc")
(countXX-1 "Hello there")
(countXX-1 "Hexxo thxxe")
(countXX-1 "")
(countXX-1 "Kittens")
(countXX-1 "Kittensxxx")

(defn countXX-2
  [palabra]
  (letfn [(f [palabra acc]
            (if (empty? palabra) acc
                (if (and (= (first palabra) \x) (= (first (rest palabra)) \x))
                  (f (rest palabra) (+ 1 acc)) (f (rest palabra) acc))))]
    (f palabra 0)))

(countXX-2 "abcxx")
(countXX-2 "xxx")
(countXX-2 "xxxx")
(countXX-2 "abc")
(countXX-2 "Hello there")
(countXX-2 "Hexxo thxxe")
(countXX-2 "")
(countXX-2 "Kittens")
(countXX-2 "Kittensxxx")